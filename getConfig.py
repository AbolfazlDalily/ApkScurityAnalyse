import configparser
class configParser():
    def __init__(self):
        self.Config = configparser.ConfigParser()
        self.Config.read('config/config.ini')
    def get_config(self):
        return self.Config

config = configParser().get_config()

from koodousApi import analyser
from pprint import pprint
from cmd import Cmd

class MyCli(Cmd):
    prompt = ">>>"
    intro = "Welcome! Type ? to list commands"

    ### Exit ####
    def do_exit(self,inp):
        print("Good Bye :)")
        return True

    def help_exit(self):
        print("exit the application. Shorthand: q Ctrl-D.")
    #############

    ### analyse ###
    def do_analyse(self,inp):
        if self.check_input(inp) :
            sha256, error = analyser.upload_apk(inp)
            if sha256 is not None:
                if analyser.analyze(sha256):
                    result, error = analyser.get_analyse(sha256)
                    pprint(result) if result is not None else print(error)
                else:
                    print("Dont Now !")
            else:
                print(error)


    def help_analyse(self):
        print("use : analyse [ apk file path ] ")

    ##############

    # def do_search(self,inp):
    #     if self.check_input(inp) :
    #         pprint(analyser.search(inp))
    #
    # def help_search(self):
    #     print("Search in apk database !")

    ########################
    def default(self, line):
        if line == 'q' :
            return self.do_exit(line)
    do_EOF = do_exit

    def check_input(self,inp):
        if inp == '' :
            print("enter command [input] ")
            return False
        else:
            return True
my_cli = MyCli()
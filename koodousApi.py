from getConfig import config
import subprocess
from os import path
import requests as req
import certifi
import time

REQUESTS_CA_BUNDLE = certifi.where()

class KoodousApi():

    def __init__(self):
        self.__token = config.get('koodous','token')
        self.__headers = {'Authorization': 'Token %s' % self.__token}
        self.__bae_url = "https://api.koodous.com/"


    def upload_apk(self,file_path):
        if path.isfile(file_path) :
            bashCommand = "sha256sum "+file_path
            proccess = subprocess.Popen(bashCommand,shell=True,stdout=subprocess.PIPE)
            out,error = proccess.communicate()
            if error is None :
                sha256 = (str(out).split("  "))[0][2:]
                url = '%s/apks/%s/get_upload_url' % (self.__bae_url, sha256)
                res = req.get(url=url, headers=self.__headers,
                                        verify=REQUESTS_CA_BUNDLE)
                if res.status_code == 200:
                    json_data = res.json()
                    # print json_data.get('upload_url', None)
                    files = {'file': open(file_path, 'rb')}

                    res = req.post(url=json_data.get("upload_url"),
                                             files=files,
                                             verify=REQUESTS_CA_BUNDLE)
                    while res.status_code == 404:  # Workaround server problem sometimes
                        time.sleep(1)
                        res = req.post(url=json_data.get("upload_url"),
                                                 files=files,
                                                 verify=REQUESTS_CA_BUNDLE)
                    return sha256,None
                elif res.status_code == 409:
                    # APK already exists
                    return sha256,None
                else:
                    return None,"Unknown error: %s" % res.text
            else:
                return None,error

        else:
            return None,"file not found "
    def get_analyse(self,sha256):
        url = '%s/apks/%s/analysis' % (self.__bae_url, sha256)

        res = req.get(url=url, headers=self.__headers,
                                verify=REQUESTS_CA_BUNDLE)
        if res.status_code == 200:
            return res.json(),None
        elif res.status_code == 405:  # Analysis doesn't exist
            return None,"This sample has not analysis available,Please try again in a few minutes."
        # Otherwise
        return None,"Unknown error: %s" % res.text

    def analyze(self, sha256):
        url = '%s/apks/%s/analyze' % (self.__bae_url, sha256)
        res = req.get(url=url, headers=self.__headers,
                           verify=REQUESTS_CA_BUNDLE)
        if res.status_code == 200:
            return True
        return False

    # def search(self,inp):
    #     params = {'search':inp}
    #     r = req.get(url="https://api.koodous.com/apks", params=params)
    #     return r.json()

analyser = KoodousApi()
